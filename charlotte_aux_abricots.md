# Charlotte aux abricots

## Ingrédients

1. 25 cl de crème fraîche liquide
2. Sucre glace
3. 25-30 boudoirs
4. Abricots (en boite)

## Préparation

- Pour la chantilly, verser la crème dans un récipient et y rajouter le sucre glace. Puis fouetter la crème jusqu'à obtenir une belle chantilly
- Séparer les abricots et le jus (verser le jus dans une assiette creuse). Couper les abricots en petits morceaux.
- Tremper la partie sans sucre des boudoirs dans le jus d'abricots et placer les dans le plat à charlotte de telle façon que le côté sucré soit vers l'extérieur.
- Remplir par couche successive de chantilly et d'abricots le plat à charlotte.
- Laisser la charlotte reposé au frigo pendant quelques heures.
- Enfin, vous pouvez la déguster.
