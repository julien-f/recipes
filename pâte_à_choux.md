# Pâte à choux

## Ingrédients

Pour une vingtaine de choux environ :

1. 1/4 de L d'eau
2. 60 g de beurre
3. 150 g de farine
4. 4 œufs

## Préparation

- Faire bouillir l'eau avec le beurre.
- Sortir du feu et ajouter toute la farine d'un coup.
- Bien mélanger à feu doux.
- Sortir du feu et ajouter progressivement les œufs.

La pâte à choux est maintenant presque terminée, pour faire des choux sucrés ajouter 30 g de sucre, pour des salés, une pincé de sel.

- Faire cuire 25 min. au four à 200 °C.
- Enfin, vous pouvez les déguster.
