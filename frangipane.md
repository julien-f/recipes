# Frangipane

La frangipane est une crème d'amande qui entre notamment dans la composition de la [[Galette des Rois]].

## Ingrédients

1. 125 g de beurre (margarine si vous préférez)
2. 125 g de sucre blanc
3. 125  g de poudre  d'amande (il  est possible de  remplacer la moitié  par des
   amandes effilées pour avoir une texture plus croquante)
4. 125 g d'œuf, soient 2 / 3 œufs (un œuf pèse à peu près 55 g)
5. 45 g de farine (à peu près un tiers de 125 g)
6. Un cuillère à café de rhum

## Préparation
- Dans un récipient, mettre le beurre  en pommade (le faire légèrement fondre et
  l'écraser avec une cuillère en bois).
- Ajouter  le sucre  et blanchir  le mélange  (bien mélanger  jusqu'à ce  que la
  couleur tire vers le blanc).
- Ajouter la poudre d'amande et mélanger.
- Incorporer progressivement les œufs.
- Ajouter la farine.
- Enfin rajouter le rhum.
