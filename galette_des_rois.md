# Galette des Rois

La  Galette  des  Rois  est  un  gâteau  célébrant  l'[[!wikipediafr Épiphanie]].  Il  est
généralement fait de pâte feuilletée fourrée de [[frangipane]].

## Galette à la frangipane

### Ingrédients

1. De la [[frangipane]]
2. 2 pâtes feuilletées
3. 1 œuf (juste pour dorer)

### Préparation
- Poser la première pâte à plat.
- La recouvrir  de [[frangipane]]  en laissant environ  2 cm  sur les
  bords.
- Dorer ces derniers avec l'œuf afin de pouvoir coller la seconde pâte.
- Ne    pas    oublier   de    rajouter    la    ou    les   fèves    dans    la
  [[frangipane]] !!!
- Poser la seconde pâte par dessus la première en collant bien les bords.
- Avec un  couteau percer la pâte du  dessus pour l'empêcher de  gonfler dans le
  four.
- Dorer la partie supérieur de la galette.
- Décorer la galette avec un couteau.
- Enfin, la faire cuire au four à 180° C pendant 30 min.
