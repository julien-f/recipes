# Gâteau de Floraline

La    Floraline<sup>[^1]</sup> est  un mélange de  semoule de
blé dur et de tapioca.

Elle est très utilisée pour l'alimentation des enfants en bas âge mais elle peut
être appréciée  par tous. Elle  peut se  manger salé, en  soupe ou en  purée par
exemple, ou sucré comme dans cette recette.

## Ingrédients

Pour 8 ramequins :

1. 500 mL de lait
2. 1 sachet de sucre vanillé
3. 2 cuillères à soupe rases de sucre
4. 8 cuillères à soupe rases de Floraline

## Préparation

- Faire bouillir le lait.
- A feu doux, ajouter la Floraline et, tout en remuant, laisser cuire 5 min.
- Ajouter le sucre puis verser dans les ramequins.
- Mettre au réfrigérateur.
- Une fois la Floraline bien solidifiée, vous pouvez déguster.

__**Idée  :**__ Avant  l'ajout du  sucre, vous  pouvez mettre  des  raisins secs
préalablement gonflés dans de l'eau chaude.

[^1]: La    Floraline    est     un    produit    de    la    société
[PastaCorp](http://www.pastacorp-export.com/).
