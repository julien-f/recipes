# Crème pâtissière

## Ingrédients
Pour 800 mL de crème :

1. 60 g de farine
1. 100 g de sucre
1. 2 œufs
1. 1/2 L de lait
1. 1 sachet de sucre vanillé (ou 1 cuillère à soupe d'extrait de vanille)

## Préparation
- Mettre un plat propre au congélateur.
- Faire  chauffer le  lait à  feu doux  avec le  sucre vanillé  ou  l'extrait de
  vanille.
- Dans le  même temps, dans un  récipient, battre les  œufs avec le sucre  et la
  farine.
- Verser progressivement un  peu de lait dans le récipient  tout en battant pour
  homogénéiser.
- Reverser le  contenu du récipient  dans la casserole  et faire chauffer  à feu
  doux tout  en mélangeant vigoureusement. Quand  la crème se  remet à bouillir,
  compter environ 2 minutes de cuisson.
- Verser la crème  dans le plat que  vous aviez placé dans le  congélateur et le
  placer au réfrigérateur.

__**Attention  :**__ La  crème pâtissière  ne se  garde pas  très  longtemps, la
consommer rapidement.

## Source
1. La [recette](http://fr.wikibooks.org/wiki/Livre_de_cuisine/Cr%C3%A8me_p%C3%A2tissi%C3%A8re) sur [Wikilivres](http://fr.wikibooks.org/).
