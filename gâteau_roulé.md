# Gâteau roulé

Ah. Mon gâteau préféré : le gâteau roulé.

Sa  tendre génoise  et sa  merveilleuse  garniture. Personnellement,  moi je  le
fourre à la  [[crème pâtissière]] mais libre  à vous de choisir
autre chose, comme du Nutella, de la confiture ou de la crème de marrons.

## Ingrédients

1. 4 œufs
2. 120 g de sucre blanc
3. 120 g de farine

## Préparation
- Séparer les jaunes des blancs des œufs.
- Blanchir  les premier  avec le  sucre  puis mélanger  progressivement avec  la
  farine.
- Battre les blancs en  neige (n'oubliez pas la pincée de sel  afin que ça monte
  bien).
- Incorporer délicatement  ces derniers dans la  pâte faite avec  les jaunes, le
  sucre et la farine.
- Étaler la préparation dans un (assez grand) moule rectangulaire  préalablement
  recouvert de papier sulfurisé.
- Faire cuire à 180°C pendant environ 12 minutes.
- Sortir  la  pâte  du  four   et  la  démouler  sur  une  serviette  humidifiée
  préalablement  (vous pouvez  la recouvrir  d'un peu  de sucre  en  poudre pour
  éviter que ça colle) et rouler le gâteau dedans.
- Attendre une minute  pour que le gâteau prenne bien la  forme puis le dérouler
  et le fourrer.
- Déguster.
